include helper
import math


proc createShaderModule*(device; shaderPath: static[string]): VkShaderModule =
  const shaderCode = slurp("../../shaders/" & shaderPath)
  var ptrShader = cast[seq[uint32]](shaderCode)
  ptrShader.setLen((shaderCode.len/4).ceil.int)
  var createInfo = VkShaderModuleCreateInfo( sType: vkStructureTypeShaderModuleCreateInfo,
    codeSize: shaderCode.len,
    pCode: ptrShader.data )
  check: device.vkCreateShaderModule(addr createInfo, nil, addr result)
