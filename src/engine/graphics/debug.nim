include helper
from strutils import format

const enableValidationLayers* = true
const validationLayers* = when enableValidationLayers: ["VK_LAYER_LUNARG_standard_validation"] else: []

proc setupGlfwDebugCallback*() =
  proc glfwDebugCallback(errorCode: cint; description: cstring) {.cdecl.} =
    debugEcho "[GLFW] $1, $2".format(errorCode, description)
  discard glfw.SetErrorCallback(glfwDebugCallback)

proc setupVulkanDebugCallback*(instance): VkDebugUtilsMessengerEXT =
  proc vulkanDebugCallback(messageSeverity: VkDebugUtilsMessageSeverityFlagBitsEXT; messageTypes: VkDebugUtilsMessageTypeFlagsEXT; pCallbackData: ptr VkDebugUtilsMessengerCallbackDataEXT; pUserData: pointer): VkBool32 {.cdecl.} =
    debugEcho "[VK] $1".format(pCallbackData[].pMessage)
    return vkFalse
  var callbackCreateInfo = VkDebugUtilsMessengerCreateInfoEXT( sType: vkStructureTypeDebugUtilsMessengerCreateInfoExt,
    messageSeverity:
      #vkDebugUtilsMessageSeverityVerboseBitExt or
      #vkDebugUtilsMessageSeverityInfoBitExt or
      vkDebugUtilsMessageSeverityWarningBitExt or
      vkDebugUtilsMessageSeverityErrorBitExt,
    messageType:
      vkDebugUtilsMessageTypeGeneralBitExt or
      vkDebugUtilsMessageTypeValidationBitExt or
      vkDebugUtilsMessageTypePerformanceBitExt,
    pfnUserCallback: vulkanDebugCallback)
  vk(vkCreateDebugUtilsMessengerEXT, instance)
  check: vvkCreateDebugUtilsMessengerEXT(instance, addr callbackCreateInfo, nil, addr result)

proc destroyVulkanDebugCallback*(instance, messenger: VkDebugUtilsMessengerEXT) =
  vk(vkDestroyDebugUtilsMessengerEXT, instance)
  vvkDestroyDebugUtilsMessengerEXT(instance, messenger, nil)

proc checkValidationLayerSupport*(validationLayers: openarray[string]): bool =
  var availableLayerCount: uint32
  check: vkEnumerateInstanceLayerProperties(addr availableLayerCount, nil)
  var availableLayers = newSeq[VkLayerProperties](availableLayerCount)
  check: vkEnumerateInstanceLayerProperties(addr availableLayerCount, availableLayers.data)
  for layer in validationLayers:
    var layerFound = false
    for availableLayer in availableLayers.mitems:
      if $cast[cstring](addr availableLayer.layerName) == layer: #Report this
        layerFound = true
    if not layerFound:
      return false
  return true
