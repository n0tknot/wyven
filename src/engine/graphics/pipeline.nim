include helper
import vertex

proc createGraphicsPipelineLayout*(device; descriptorSetLayouts: seq[VkDescriptorSetLayout]): VkPipelineLayout =
  var pipelineLayoutInfo = VkPipelineLayoutCreateInfo( sType: vkStructureTypePipelineLayoutCreateInfo,
    pSetLayouts: descriptorSetLayouts.data, setLayoutCount: descriptorSetLayouts.len.uint32,
    pushConstantRangeCount: 0 )
  check: device.vkCreatePipelineLayout(addr pipelineLayoutInfo, nil, addr result)

proc createGraphicsPipeline*(device, pipelineLayout, renderPass; extent: VkExtent2D, vertShaderModule, fragShaderModule: VkShaderModule): VkPipeline =
  var vertShaderStageInfo = VkPipelineShaderStageCreateInfo( sType: vkStructureTypePipelineShaderStageCreateInfo,
    stage: vkShaderStageVertexBit,
    module: vertShaderModule,
    pName: "main" )
  var fragShaderStageInfo = VkPipelineShaderStageCreateInfo( sType: vkStructureTypePipelineShaderStageCreateInfo,
    stage: vkShaderStageFragmentBit,
    module: fragShaderModule,
    pName: "main" )
  var shaderStages = @[
    vertShaderStageInfo,
    fragShaderStageInfo ]
  var bindingDescription = getBindingDescription()
  var attributeDescriptions = getAttributeDescriptions()
  var vertexInputState = VkPipelineVertexInputStateCreateInfo( sType: vkStructureTypePipelineVertexInputStateCreateInfo,
    pVertexBindingDescriptions: addr bindingDescription, vertexBindingDescriptionCount: 1,
    pVertexAttributeDescriptions: attributeDescriptions.data, vertexAttributeDescriptionCount: attributeDescriptions.len.uint32 )
  #TODO optimize?
  var inputAssemblyState = VkPipelineInputAssemblyStateCreateInfo( sType: vkStructureTypePipelineInputAssemblyStateCreateInfo,
    topology: vkPrimitiveTopologyTriangleList,
    primitiveRestartEnable: vkFalse )
  var viewport = VkViewport(
    x: 0.0,
    y: 0.0,
    width: extent.width.cfloat,
    height: extent.height.cfloat,
    minDepth: 0.0,
    maxDepth: 1.0 )
  var scissor = VkRect2D(
    offset: VkOffset2D(
      x: 0,
      y: 0 ),
    extent: extent )
  var viewportState = VkPipelineViewportStateCreateInfo( sType: vkStructureTypePipelineViewportStateCreateInfo,
    pViewports: addr viewport, viewportCount: 1,
    pScissors: addr scissor, scissorCount: 1 )
  var rasterizationState = VkPipelineRasterizationStateCreateInfo( sType: vkStructureTypePipelineRasterizationStateCreateInfo,
    depthClampEnable: vkFalse,
    rasterizerDiscardEnable: vkFalse,
    polygonMode: vkPolygonModeFill,
    lineWidth: 1.0,
    cullMode: vkCullModeBackBit,
    frontFace: vkFrontFaceClockwise,
    depthBiasEnable: vkFalse )
  var multisampleState = VkPipelineMultisampleStateCreateInfo( sType: vkStructureTypePipelineMultisampleStateCreateInfo,
    sampleShadingEnable: vkFalse,
    rasterizationSamples: vkSampleCount_1Bit )
  var colorBlendAttachment = VkPipelineColorBlendAttachmentState(
    colorWriteMask:
      vkColorComponentRBit or
      vkColorComponentGBit or
      vkColorComponentBBit or
      vkColorComponentABit,
    blendEnable: vkFalse ) #for now no blending
  var colorBlendState = VkPipelineColorBlendStateCreateInfo( sType: vkStructureTypePipelineColorBlendStateCreateInfo,
    logicOpEnable: vkFalse,
    logicOp: vkLogicOpCopy,
    pAttachments: addr colorBlendAttachment, attachmentCount: 1,
    blendConstants: [0.0f, 0.0f, 0.0f, 0.0f] )
  var pipelineInfo = VkGraphicsPipelineCreateInfo( sType: vkStructureTypeGraphicsPipelineCreateInfo,
    pStages: shaderStages.data, stageCount: 2,
    pVertexInputState: addr vertexInputState,
    pInputAssemblyState: addr inputAssemblyState,
    pViewportState: addr viewportState,
    pRasterizationState: addr rasterizationState,
    pMultisampleState: addr multisampleState,
    pColorBlendState: addr colorBlendState,
    layout: pipelineLayout,
    renderPass: renderPass,
    subpass: 0 )
  check: device.vkCreateGraphicsPipelines(vkNullHandle, 1, addr pipelineInfo, nil, addr result)

