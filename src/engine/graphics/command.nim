include helper
import profile


from test import indices #HACK
from setup import QueueFamilies

proc createCommandPool*(device; queueFamilies: QueueFamilies): VkCommandPool =
  var commandPoolInfo = VkCommandPoolCreateInfo( sType: vkStructureTypeCommandPoolCreateInfo,
    queueFamilyIndex: queueFamilies[0] )
  check: device.vkCreateCommandPool(addr commandPoolInfo, nil, addr result)

proc createCommandBuffers*(device, commandPool, renderPass, pipeline, pipelineLayout;
                           extent: VkExtent2D, framebuffers: seq[VkFramebuffer], descriptorSets: seq[VkDescriptorSet], vertexBuffer, indexBuffer: VkBuffer, queryPool: VkQueryPool = 0): seq[VkCommandBuffer] =
  result.setLen(framebuffers.len)
  var allocInfo = VkCommandBufferAllocateInfo( sType: vkStructureTypeCommandBufferAllocateInfo,
    level: vkCommandBufferLevelPrimary,
    commandPool: commandPool,
    commandBufferCount: result.len.uint32 )
  check: device.vkAllocateCommandBuffers(addr allocInfo, result.data)
  for index, commandBuffer in result:
    var beginInfo = VkCommandBufferBeginInfo( sType: vkStructureTypeCommandBufferBeginInfo )
    check: commandBuffer.vkBeginCommandBuffer(addr beginInfo)
    var clearColor = VkClearValue(
      color: VkClearColorValue(
        float32: [0'f, 0, 0, 1] ) )
    when enableProfiling:
      commandBuffer.vkCmdResetQueryPool(queryPool, 0, 2)
    var renderPassBeginInfo = VkRenderPassBeginInfo( sType: vkStructureTypeRenderPassBeginInfo,
      renderPass: renderPass,
      framebuffer: framebuffers[index],
      renderArea: VkRect2D(
        offset: VkOffset2D(
          x: 0,
          y: 0 ),
        extent: extent ),
      pClearValues: addr clearColor, clearValueCount: 1 )
    commandBuffer.vkCmdBeginRenderPass(addr renderPassBeginInfo, vkSubpassContentsInline)
    commandBuffer.vkCmdBindPipeline(vkPipelineBindPointGraphics, pipeline)
    when enableProfiling:
      commandBuffer.vkCmdWriteTimestamp(vkPipelineStageTopOfPipeBit, queryPool, 0)
    var vertexBuffers = [vertexBuffer]
    var offsets = [0.VkDeviceSize]
    commandBuffer.vkCmdBindVertexBuffers(0'u32, 1'u32, vertexBuffers.data, offsets.data)
    commandBuffer.vkCmdBindIndexBuffer(indexBuffer, 0, vkIndexTypeUint16)
    commandBuffer.vkCmdBindDescriptorSets(vkPipelineBindPointGraphics, pipelineLayout, 0, 1, unsafeAddr descriptorSets[index], 0, nil)
    commandBuffer.vkCmdDrawIndexed(indices.len.uint32, 1, 0, 0, 0)
    when enableProfiling:
      commandBuffer.vkCmdWriteTimestamp(vkPipelineStageBottomOfPipeBit, queryPool, 1)
    commandBuffer.vkCmdEndRenderPass()
    check: commandBuffer.vkEndCommandBuffer()

proc beginSingleTimeCommands*(device, commandPool): VkCommandBuffer =
  var allocInfo = VkCommandBufferAllocateInfo( sType: vkStructureTypeCommandBufferAllocateInfo,
    level: vkCommandBufferLevelPrimary,
    commandPool: commandPool,
    commandBufferCount: 1 )
  check: device.vkAllocateCommandBuffers(addr allocInfo, addr result)
  var beginInfo = VkCommandBufferBeginInfo( sType: vkStructureTypeCommandBufferBeginInfo,
    flags: vkCommandBufferUsageOneTimeSubmitBit )
  check: result.vkBeginCommandBuffer(addr beginInfo)

proc endSingleTimeCommands*(device, queue: VkQueue, commandPool; commandBuffer: var VkCommandBuffer) =
  check: commandBuffer.vkEndCommandBuffer()
  var submitInfo = VkSubmitInfo( sType: vkStructureTypeSubmitInfo,
    pCommandBuffers: addr commandBuffer, commandBufferCount: 1 )
  check: vkQueueSubmit(queue, 1, addr submitInfo, vkNullHandle)
  check: vkQueueWaitIdle(queue)
  device.vkFreeCommandBuffers(commandPool, 1, addr commandBuffer)
