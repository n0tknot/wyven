include helper
import ../vector

type
  Vertex* = object
    pos*: Vec2
    texCoord*: Vec2


proc getBindingDescription*(): VkVertexInputBindingDescription =
  VkVertexInputBindingDescription(
    binding: 0,
    stride: sizeof(Vertex).uint32,
    inputRate: vkVertexInputRateVertex )

proc getAttributeDescriptions*(): array[2, VkVertexInputAttributeDescription] =
  result = [
    VkVertexInputAttributeDescription(
      binding: 0,
      location: 0,
      format: vkFormatr32g32Sfloat, #Position consists of 2 float32
      offset: offsetof(Vertex, pos).uint32 ),
    VkVertexInputAttributeDescription(
      binding: 0,
      location: 1,
      format: vkFormatr32g32Sfloat, #Position consists of 2 float32
      offset: offsetof(Vertex, texCoord).uint32 ) ]
