include helper
from image import createImageView

proc pickFormat*(formats: seq[VkSurfaceFormatKHR]): VkSurfaceFormatKHR =
  if formats.len == 1 and formats[0].format == vkFormatUndefined:
    return VkSurfaceFormatKHR(format: vkFormatB8G8R8A8Unorm, colorSpace: vkColorSpaceSrgbNonlinearKhr)
  for format in formats:
    if format.format == vkFormatB8G8R8A8Unorm and format.colorSpace == vkColorSpaceSrgbNonlinearKhr:
      return format
  return formats[0] #TODO maybe rank different surface formats

proc pickPresentMode*(presentModes: seq[VkPresentModeKHR]): VkPresentModeKHR =
  var bestMode = vkPresentModeFifoKhr #TODO what?
  for presentMode in presentModes:
    case presentMode
    of vkPresentModeMailboxKhr:
      return presentMode
    of vkPresentModeImmediateKhr:
      bestMode = presentMode
    else:
      discard
  return bestMode

proc pickExtent*(swapchainCaps: VkSurfaceCapabilitiesKHR, window: glfw.Window): VkExtent2D =
  if swapchainCaps.currentExtent.width != uint32.high: #and swapchainCaps.currentExtent.height != uint32.high
    return swapchainCaps.currentExtent
  else:
    var width, height: cint
    #Wait until not minimized anymore
    while width == 0 or height == 0:
      glfw.GetFramebufferSize(window, addr width, addr height)
      glfw.WaitEvents()
    glfw.GetFramebufferSize(window, addr width, addr height)
    return VkExtent2D(
      width: max(swapchainCaps.minImageExtent.width, min(swapchainCaps.maxImageExtent.width, width.uint32)),
      height: max(swapchainCaps.minImageExtent.height, min(swapchainCaps.maxImageExtent.height, height.uint32)) )

proc pickSwapImageCount*(swapchainCaps: VkSurfaceCapabilitiesKHR): uint32 =
  result = swapchainCaps.minImageCount + 1 #Add one, so we won't wait on the driver
  if swapchainCaps.maxImageCount > 0'u32 and result > swapchainCaps.maxImageCount:
    result = swapchainCaps.maxImageCount

proc createSwapchain*(device, surface; swapchainCaps: VkSurfaceCapabilitiesKHR, format: VkSurfaceFormatKHR, presentMode: VkPresentModeKHR, extent: VkExtent2D, imageCount: uint32): VkSwapchainKHR =
  var createInfo = VkSwapchainCreateInfoKHR( sType: vkStructureTypeSwapchainCreateInfoKhr,
    surface: surface,
    minImageCount: imageCount,
    imageFormat: format.format,
    imageColorSpace: format.colorSpace,
    imageExtent: extent,
    imageArrayLayers: 1,
    imageUsage: vkImageUsageColorAttachmentBit,
    imageSharingMode: vkSharingModeExclusive,
    compositeAlpha: vkCompositeAlphaOpaqueBitKhr,
    preTransform: swapchainCaps.currentTransform,
    presentMode: presentMode,
    clipped: vkTrue )
  #[ TODO: Deal with this, if some hardware exists that requires it
  if queueFamilies[0] != queueFamilies[1]: #Graphics queue family is not the same as present queue family
    echo "Graphics queue family not same as present"
    createInfo.imageSharingMode = vkSharingModeConcurrent
    createInfo.pQueueFamilyIndices = queueFamilies.data
    createInfo.queueFamilyIndexCount = queueFamilies.len.uint32  ]#
  check: device.vkCreateSwapchainKHR(addr createInfo, nil, addr result)


proc getSwapchainImages*(device, swapchain): seq[VkImage] =
  var imageCount: uint32
  check: device.vkGetSwapchainImagesKHR(swapchain, addr imageCount, nil)
  result.setLen(imageCount)
  check: device.vkGetSwapchainImagesKHR(swapchain, addr imageCount, result.data)

proc createImageViews*(device; swapchainImages: seq[VkImage], format: VkFormat): seq[VkImageView] =
  result.setLen(swapchainImages.len)
  for index, swapchainImage in swapchainImages:
    result[index] = device.createImageView(swapchainImage, format)

proc createFramebuffers*(device, renderPass; extent: VkExtent2D, swapchainImageViews: seq[VkImageView]): seq[VkFramebuffer] =
  result.setLen(swapchainImageViews.len)
  for index, swapchainImageView in swapchainImageViews:
    var attachments = [swapchainImageView] #Since we only have one attachment, we actually need no array here
    var framebufferInfo = VkFramebufferCreateInfo( sType: vkStructureTypeFramebufferCreateInfo,
      renderPass: renderPass,
      pAttachments: attachments.data, attachmentCount: 1,
      width: extent.width,
      height: extent.height,
      layers: 1 )
    check: device.vkCreateFramebuffer(addr framebufferInfo, nil, addr result[index])
