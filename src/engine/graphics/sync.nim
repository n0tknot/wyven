include helper

#To optim. pull create info out of body
proc createSemaphore*(device): VkSemaphore =
  var semaphoreInfo = VkSemaphoreCreateInfo( sType: vkStructureTypeSemaphoreCreateInfo )
  check: device.vkCreateSemaphore(addr semaphoreInfo, nil, addr result)

proc createFence*(device): VkFence =
  var fenceInfo = VkFenceCreateInfo( sType: vkStructureTypeFenceCreateInfo,
    flags: vkFenceCreateSignaledBit ) #Create in signaled state
  check: device.vkCreateFence(addr fenceInfo, nil, addr result)

