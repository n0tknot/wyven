include helper
from sequtils import newSeqWith


proc createDescriptorSetLayout*(device): VkDescriptorSetLayout =
  var bindings = [
    VkDescriptorSetLayoutBinding( #Uniform buffer object
      binding: 0,
      descriptorType: vkDescriptorTypeUniformBuffer,
      descriptorCount: 1,
      stageFlags: vkShaderStageVertexBit ), #Will be used in vertex shader
    VkDescriptorSetLayoutBinding( #An image sampler
      binding: 1,
      descriptorType: vkDescriptorTypeCombinedImageSampler,
      descriptorCount: 1,
      stageFlags: vkShaderStageFragmentBit ) ] #Will be used in fragment shader (texture sampling)
  var layoutInfo = VkDescriptorSetLayoutCreateInfo( sType: vkStructureTypeDescriptorSetLayoutCreateInfo,
    pBindings: bindings.data, bindingCount: bindings.len.uint32 )
  check: device.vkCreateDescriptorSetLayout(addr layoutInfo, nil, addr result)

proc createDescriptorPool*(device; count: uint32): VkDescriptorPool =
  var poolSizes = [
    VkDescriptorPoolSize( #Uniform buffer object
      `type`: vkDescriptorTypeUniformBuffer,
      descriptorCount: count ),
    VkDescriptorPoolSize( #An image sampler
      `type`: vkDescriptorTypeCombinedImageSampler,
      descriptorCount: count ) ]
  var poolInfo = VkDescriptorPoolCreateInfo( sType: vkStructureTypeDescriptorPoolCreateInfo,
    pPoolSizes: poolSizes.data, poolSizeCount: poolSizes.len.uint32,
    maxSets: count )
  check: device.vkCreateDescriptorPool(addr poolInfo, nil, addr result)

proc createDescriptorSets*(device; descriptorSetLayout: VkDescriptorSetLayout, descriptorPool: VkDescriptorPool, uniformBuffers: seq[VkBuffer], textureImageView: VkImageView, textureSampler: VkSampler, count: int): seq[VkDescriptorSet] =
  result.setLen(count)
  var layouts: seq[VkDescriptorSetLayout] = newSeqWith(count, descriptorSetLayout)
  var allocInfo = VkDescriptorSetAllocateInfo( sType: vkStructureTypeDescriptorSetAllocateInfo,
    descriptorPool: descriptorPool,
    pSetLayouts: layouts.data, descriptorSetCount: layouts.len.uint32 )
  check: device.vkAllocateDescriptorSets(addr allocInfo, result.data)
  for i in 0..<count:
    var bufferInfo = VkDescriptorBufferInfo(
      buffer: uniformBuffers[i],
      offset: 0,
      range: #[vkWholeSize]# 8 ) #Should use size of UniformBufferObject
    var imageInfo = VkDescriptorImageInfo( #Attach an image
      imageLayout: vkImageLayoutShaderReadOnlyOptimal,
      imageView: textureImageView, #w8 a minute... these are not arguments?
      sampler: textureSampler )
    var descriptorWrites = [
      VkWriteDescriptorSet( sType: vkStructureTypeWriteDescriptorSet,
        dstSet: result[i],
        dstBinding: 0,
        dstArrayElement: 0,  #Our descriptors are not in an array
        descriptorType: vkDescriptorTypeUniformBuffer,
        descriptorCount: 1,  #Our descriptors are not in an array/we only update one descriptor
        pBufferInfo: addr bufferInfo ),
      VkWriteDescriptorSet( sType: vkStructureTypeWriteDescriptorSet,
        dstSet: result[i],
        dstBinding: 1,
        dstArrayElement: 0,  #Our descriptors are not in an array
        descriptorType: vkDescriptorTypeCombinedImageSampler,
        descriptorCount: 1,  #Our descriptors are not in an array/we only update one descriptor
        pImageInfo: addr imageInfo ) ]
    device.vkUpdateDescriptorSets(descriptorWrites.len.uint32, descriptorWrites.data, 0, nil)

