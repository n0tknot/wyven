include helper

proc createRenderPass*(device; format: VkFormat): VkRenderPass =
  var colorAttachment = VkAttachmentDescription(
    format: format,
    samples: vkSampleCount_1Bit,
    loadOp: vkAttachmentLoadOpClear,
    storeOp: vkAttachmentStoreOpStore,
    stencilLoadOp: vkAttachmentLoadOpDontCare,
    stencilStoreOp: vkAttachmentStoreOpDontCare,
    initialLayout: vkImageLayoutUndefined,
    finalLayout: vkImageLayoutPresentSrcKHR )
  var colorAttachmentRef = VkAttachmentReference(
    attachment: 0,
    layout: vkImageLayoutColorAttachmentOptimal )
  var subpass = VkSubpassDescription(
    pipelineBindPoint: vkPipelineBindPointGraphics,
    pColorAttachments: addr colorAttachmentRef, colorAttachmentCount: 1 )
  var subpassDependency = VkSubpassDependency(
    srcSubpass: vkSubpassExternal,
    dstSubpass: 0,
    srcStageMask: vkPipelineStageColorAttachmentOutputBit,
    srcAccessMask: 0,
    dstStageMask: vkPipelineStageColorAttachmentOutputBit,
    dstAccessMask: vkAccessColorAttachmentReadBit or vkAccessColorAttachmentWriteBit )
  var renderPassInfo = VkRenderPassCreateInfo( sType: vkStructureTypeRenderPassCreateInfo,
    pAttachments: addr colorAttachment, attachmentCount: 1,
    pSubpasses: addr subpass, subpassCount: 1,
    pDependencies: addr subpassDependency, dependencyCount: 1 )
  check: device.vkCreateRenderPass(addr renderPassInfo, nil, addr result)

