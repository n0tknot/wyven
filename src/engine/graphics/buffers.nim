include helper
import vertex
import command


proc pickMemoryType*(physicalDevice; typeFilter: uint32, properties: VkMemoryPropertyFlags): uint32 =
  var memoryProperties: VkPhysicalDeviceMemoryProperties
  physicalDevice.vkGetPhysicalDeviceMemoryProperties(addr memoryProperties)
  for i in 0..memoryProperties.memoryTypeCount:
    if (typeFilter and (1 shl i).uint32) != 0 and (memoryProperties.memoryTypes[i].propertyFlags and properties) == properties:
      return i
  quit "Failed to find suitable memory type"

proc createBuffer*(device, physicalDevice; size: VkDeviceSize, usage: VkBufferUsageFlags, properties: VkMemoryPropertyFlags): tuple[
    buffer: VkBuffer,
    bufferMemory: VkDeviceMemory ] =
  var bufferInfo = VkBufferCreateInfo( sType: vkStructureTypeBufferCreateInfo,
    size: size,
    usage: usage,
    sharingMode: vkSharingModeExclusive )
  check: device.vkCreateBuffer(addr bufferInfo, nil, addr result.buffer)
  var memoryRequirements: VkMemoryRequirements
  device.vkGetBufferMemoryRequirements(result.buffer, addr memoryRequirements)
  var allocInfo = VkMemoryAllocateInfo( sType: vkStructureTypeMemoryAllocateInfo,
    allocationSize: memoryRequirements.size,
    memoryTypeIndex: physicalDevice.pickMemoryType(memoryRequirements.memoryTypeBits, properties) )
  check: device.vkAllocateMemory(addr allocInfo, nil, addr result.bufferMemory)
  discard device.vkBindBufferMemory(result.buffer, result.bufferMemory, 0)

proc copyBuffer*(device, commandPool; graphicQueue: VkQueue, src, dst: VkBuffer, size: VkDeviceSize) =
  var commandBuffer = device.beginSingleTimeCommands(commandPool)
  var copyRegion = VkBufferCopy( size: size )
  vkCmdCopyBuffer(commandBuffer, src, dst, 1, addr copyRegion)
  device.endSingleTimeCommands(graphicQueue, commandPool, commandBuffer)

proc createVertexBuffer*(device, physicalDevice, commandPool; graphicQueue: VkQueue, vertices: var seq[Vertex]): tuple[
    buffer: VkBuffer, bufferMemory: VkDeviceMemory ] =
  var bufferSize = (sizeof(vertices[0]) * vertices.len).VkDeviceSize
  var stagingBuffer: VkBuffer
  var stagingBufferMemory: VkDeviceMemory
  (stagingBuffer, stagingBufferMemory) = device.createBuffer(physicalDevice, bufferSize, vkBufferUsageTransferSrcBit, vkMemoryPropertyHostVisibleBit or vkMemoryPropertyHostCoherentBit)
  var data: pointer
  discard device.vkMapMemory(stagingBufferMemory, 0, bufferSize, 0, addr data)
  copyMem(data, vertices.data, bufferSize)
  device.vkUnmapMemory(stagingBufferMemory)
  result = device.createBuffer(physicalDevice, bufferSize, vkBufferUsageTransferDstBit or vkBufferUsageVertexBufferBit, vkMemoryPropertyDeviceLocalBit)
  device.copyBuffer(commandPool, graphicQueue, stagingBuffer, result.buffer, bufferSize)
  device.vkDestroyBuffer(stagingBuffer, nil)
  device.vkFreeMemory(stagingBufferMemory, nil)

proc createIndexBuffer*(device, physicalDevice, commandPool; graphicQueue: VkQueue, indices: var seq[uint16]): tuple[
    buffer: VkBuffer,
    bufferMemory: VkDeviceMemory ] =
  var bufferSize = (sizeof(indices[0]) * indices.len).VkDeviceSize
  var stagingBuffer: VkBuffer
  var stagingBufferMemory: VkDeviceMemory
  (stagingBuffer, stagingBufferMemory) = device.createBuffer(physicalDevice, bufferSize, vkBufferUsageTransferSrcBit, vkMemoryPropertyHostVisibleBit or vkMemoryPropertyHostCoherentBit)
  var data: pointer
  discard device.vkMapMemory(stagingBufferMemory, 0, bufferSize, 0, addr data)
  copyMem(data, indices.data, bufferSize)
  device.vkUnmapMemory(stagingBufferMemory)
  result = device.createBuffer(physicalDevice, bufferSize, vkBufferUsageTransferDstBit or vkBufferUsageIndexBufferBit, vkMemoryPropertyDeviceLocalBit)
  device.copyBuffer(commandPool, graphicQueue, stagingBuffer, result.buffer, bufferSize)
  device.vkDestroyBuffer(stagingBuffer, nil)
  device.vkFreeMemory(stagingBufferMemory, nil)

proc createUniformBuffers*(device, physicalDevice; bufferSize: int, count: int): tuple[
    buffers: seq[VkBuffer],
    buffersMemory: seq[VkDeviceMemory] ] =
  result.buffers.setLen(count)
  result.buffersMemory.setLen(count)
  for i in 0..<count:
    (result.buffers[i],
     result.buffersMemory[i]) = device.createBuffer(physicalDevice, bufferSize.VkDeviceSize, vkBufferUsageUniformBufferBit, vkMemoryPropertyHostVisibleBit or vkMemoryPropertyHostCoherentBit)

proc updateUniformBuffer*[T](device; uniformBuffersMemory: VkDeviceMemory, uniformBufferRAM: var T) =
  var data: pointer
  discard device.vkMapMemory(uniformBuffersMemory, 0, sizeof(T).VkDeviceSize, 0, addr data)
  copyMem(data, addr uniformBufferRAM, sizeof(T))
  device.vkUnmapMemory(uniformBuffersMemory)
