include helper
import stb_image/read as stbi
from command import beginSingleTimeCommands, endSingleTimeCommands
from buffers import pickMemoryType, createBuffer

proc transitionImageLayout(device, commandPool; graphicQueue: VkQueue, image: VkImage, format: VkFormat, oldLayout, newLayout: VkImageLayout) =
  var commandBuffer = device.beginSingleTimeCommands(commandPool)
  var barrier = VkImageMemoryBarrier( sType: vkStructureTypeImageMemoryBarrier,
    oldLayout: oldLayout,
    newLayout: newLayout,
    srcQueueFamilyIndex: vkQueueFamilyIgnored, #No transfer of queue family ownership here, so we use vkQueueFamilyIgnored
    dstQueueFamilyIndex: vkQueueFamilyIgnored,
    subresourceRange: VkImageSubresourceRange(
      aspectMask: vkImageAspectColorBit,
      baseMipLevel: 0, levelCount: 1,
      baseArrayLayer: 0, layerCount: 1 ),
    image: image,
    srcAccessMask: case oldLayout
                   of vkImageLayoutUndefined: 0.VkAccessFlagBits
                   of vkImageLayoutTransferDstOptimal: vkAccessTransferWriteBit
                   else: 0.VkAccessFlagBits, #Should probably raise here
    dstAccessMask: case newLayout
                   of vkImageLayoutTransferDstOptimal: vkAccessTransferWriteBit
                   of vkImageLayoutShaderReadOnlyOptimal: vkAccessShaderReadBit
                   else: 0.VkAccessFlagBits )
  let sourceStage = case oldLayout
                    of vkImageLayoutUndefined: vkPipelineStageTopOfPipeBit
                    of vkImageLayoutTransferDstOptimal: vkPipelineStageTransferBit
                    else: 0.VkPipelineStageFlagBits
  let destinStage = case newLayout
                    of vkImageLayoutTransferDstOptimal: vkPipelineStageTransferBit
                    of vkImageLayoutShaderReadOnlyOptimal: vkPipelineStageFragmentShaderBit
                    else: 0.VkPipelineStageFlagBits
  commandBuffer.vkCmdPipelineBarrier(sourceStage, destinStage, 0, 0, nil, 0, nil, 1, addr barrier)
  device.endSingleTimeCommands(graphicQueue, commandPool, commandBuffer)

proc copyBufferToImage(device, commandPool; graphicQueue: VkQueue, buffer: VkBuffer, image: VkImage, width, height: int) =
  var commandBuffer = device.beginSingleTimeCommands(commandPool)
  var region = VkBufferImageCopy(
    bufferOffset: 0,
    bufferRowLength: 0,
    bufferImageHeight: 0,
    imageSubresource: VkImageSubresourceLayers(
      aspectMask: vkImageAspectColorBit,
      mipLevel: 0,
      baseArrayLayer: 0, layerCount: 1 ),
    imageOffset: VkOffset3D( x: 0, y: 0, z: 0 ),
    imageExtent: VkExtent3D(
      width: width.uint32,
      height: height.uint32,
      depth: 1 ) )
  commandBuffer.vkCmdCopyBufferToImage(buffer, image, vkImageLayoutTransferDstOptimal, 1, addr region)
  device.endSingleTimeCommands(graphicQueue, commandPool, commandBuffer)

proc createImage*(device, physicalDevice; width, height: int, format: VkFormat, tiling: VkImageTiling, usage: VkImageUsageFlags, properties: VkMemoryPropertyFlags): tuple[
    image: VkImage,
    imageMemory: VkDeviceMemory ] =
  var imageInfo = VkImageCreateInfo( sType: vkStructureTypeImageCreateInfo,
    imageType: vkImageType2D,
    extent: VkExtent3D(
      width: width.uint32,
      height: height.uint32,
      depth: 1 ),
    mipLevels: 1,
    arrayLayers: 1,
    format: vkFormatR8G8B8A8Unorm, #Argument needed?
    tiling: vkImageTilingOptimal, #Argument needed?
    initialLayout: vkImageLayoutUndefined, #since we are going buffer -> image
    usage: vkImageUsageTransferDstBit or vkImageUsageSampledBit, #since we use this in the fragment shader
    sharingMode: vkSharingModeExclusive,
    samples: vkSampleCount1Bit )
  check: device.vkCreateImage(addr imageInfo, nil, addr result.image)
  var memoryRequirements: VkMemoryRequirements
  device.vkGetImageMemoryRequirements(result.image, addr memoryRequirements)
  var allocInfo = VkMemoryAllocateInfo( sType: vkStructureTypeMemoryAllocateInfo,
    allocationSize: memoryRequirements.size,
    memoryTypeIndex: physicalDevice.pickMemoryType(memoryRequirements.memoryTypeBits, vkMemoryPropertyDeviceLocalBit) )
  check: device.vkAllocateMemory(addr allocInfo, nil, addr result.imageMemory)
  discard device.vkBindImageMemory(result.image, result.imageMemory, 0)

proc destroyImage*(device; image: VkImage, imageMemory: VkDeviceMemory) =
  device.vkDestroyImage(image, nil)
  device.vkFreeMemory(imageMemory, nil)

proc createTextureImage*(device, physicalDevice, commandPool; graphicQueue: VkQueue): tuple[
    image: VkImage,
    imageMemory: VkDeviceMemory ] =
  var width, height, channels: int
  var pixels: seq[uint8]
  pixels = stbi.load("res/gfx/test.png", width, height, channels, stbi.RGBA)
  var size = (width * height * 4).VkDeviceSize #in bytes
  #We use buffer -> image here
  var (stagingBuffer, stagingBufferMemory) = device.createBuffer(physicalDevice, size, vkBufferUsageTransferSrcBit, vkMemoryPropertyHostVisibleBit or vkMemoryPropertyHostCoherentBit)
  var data: pointer
  discard device.vkMapMemory(stagingBufferMemory, 0, size, 0, addr data)
  copyMem(data, pixels.data, size)
  device.vkUnmapMemory(stagingBufferMemory)
  result = device.createImage(physicalDevice, width, height, vkFormatR8G8B8A8Unorm, vkImageTilingOptimal, vkImageUsageTransferDstBit or vkImageUsageSampledBit, vkMemoryPropertyDeviceLocalBit)
  device.transitionImageLayout(commandPool, graphicQueue, result.image, vkFormatR8G8B8A8Unorm, vkImageLayoutUndefined, vkImageLayoutTransferDstOptimal)
  device.copyBufferToImage(commandPool, graphicQueue, stagingBuffer, result.image, width, height)
  device.transitionImageLayout(commandPool, graphicQueue, result.image, vkFormatR8G8B8A8Unorm, vkImageLayoutTransferDstOptimal, vkImageLayoutShaderReadOnlyOptimal) #So we can use it in a shader
  device.vkDestroyBuffer(stagingBuffer, nil)
  device.vkFreeMemory(stagingBufferMemory, nil)

proc createImageView*(device; image: VkImage, format: VkFormat): VkImageView =
  var viewInfo = VkImageViewCreateInfo( sType: vkStructureTypeImageViewCreateInfo,
    image: image,
    viewType: vkImageViewType2D,
    format: format,
    components: VkComponentMapping( #Not really neccesary, since vkComponentSwizzleIdentity = 0 anyways
      r: vkComponentSwizzleIdentity,
      g: vkComponentSwizzleIdentity,
      b: vkComponentSwizzleIdentity,
      a: vkComponentSwizzleIdentity ),
    subresourceRange: VkImageSubresourceRange(
      aspectMask: vkImageAspectColorBit,
      baseMipLevel: 0, levelCount: 1,
      baseArrayLayer: 0, layerCount: 1 ) )
  check: device.vkCreateImageView(addr viewInfo, nil, addr result)

proc createTextureImageView*(device; image: VkImage): VkImageView =
  device.createImageView(image, vkFormatR8G8B8A8Unorm)

proc createTextureSampler*(device; ): VkSampler =
  var samplerInfo = VkSamplerCreateInfo( sType: vkStructureTypeSamplerCreateInfo,
    magFilter: vkFilterNearest,
    minFilter: vkFilterNearest,
    addressModeU: vkSamplerAddressModeRepeat,
    addressModeV: vkSamplerAddressModeRepeat,
    addressModeW: vkSamplerAddressModeRepeat,
    anisotropyEnable: vkFalse,
    maxAnisotropy: 1,
    borderColor: vkBorderColorIntOpaqueBlack, #This doesn't do anything if have address mode is on repeat
    unnormalizedCoordinates: vkFalse,
    compareEnable: vkFalse,
    compareOp: vkCompareOpAlways,
    mipmapMode: vkSamplerMipmapModeLinear,
    mipLodBias: 0'f,
    minLod: 0'f,
    maxLod: 0'f )
  check: device.vkCreateSampler(addr samplerInfo, nil, addr result)
