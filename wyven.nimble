# Package
version       = "0.1.0"
author        = "Clyybber"
description   = "wyven - a game"
license       = "GPLv3"

# Structure
srcDir = "src"
binDir = "bin"
bin    = @["wyven"]

# Dependencies handled via submodules
